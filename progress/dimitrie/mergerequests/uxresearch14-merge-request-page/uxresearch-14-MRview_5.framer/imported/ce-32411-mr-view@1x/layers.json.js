window.__imported__ = window.__imported__ || {};
window.__imported__["ce-32411-mr-view@1x/layers.json.js"] = [
	{
		"objectId": "3FC6128C-4466-40A2-88CD-469F3B0555D7",
		"kind": "artboard",
		"name": "topbar",
		"originalName": "topbar",
		"maskFrame": null,
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 1280,
			"height": 102
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "FC258AA3-03A0-4527-B932-F8D014A6279C",
				"kind": "group",
				"name": "top",
				"originalName": "top",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 1280,
					"height": 102
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-top-rkmynthb.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 1280,
						"height": 102
					}
				},
				"children": []
			}
		]
	},
	{
		"objectId": "1184CA39-78B7-4F54-9D9A-EC83622300AC",
		"kind": "artboard",
		"name": "sidebar",
		"originalName": "sidebar",
		"maskFrame": null,
		"layerFrame": {
			"x": 2360,
			"y": 102,
			"width": 290,
			"height": 698
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "5FAA385F-B9DE-48E9-9E14-347B41636048",
				"kind": "group",
				"name": "side",
				"originalName": "side",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 290,
					"height": 1872
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-side-nuzbqtm4.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 290,
						"height": 1872
					}
				},
				"children": [
					{
						"objectId": "45482356-0B48-4ADD-98DC-3E617A318068",
						"kind": "group",
						"name": "todo",
						"originalName": "todo",
						"maskFrame": null,
						"layerFrame": {
							"x": 18,
							"y": 10,
							"width": 259,
							"height": 46
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-todo-ndu0odiz.png",
							"frame": {
								"x": 18,
								"y": 10,
								"width": 259,
								"height": 46
							}
						},
						"children": [
							{
								"objectId": "D96EE55B-BDF1-4990-B095-52F7E88F72C1",
								"kind": "group",
								"name": "grey_button_default",
								"originalName": "grey-button-default",
								"maskFrame": null,
								"layerFrame": {
									"x": 158,
									"y": 10,
									"width": 83,
									"height": 35
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-grey_button_default-rdk2ruu1.png",
									"frame": {
										"x": 158,
										"y": 10,
										"width": 83,
										"height": 35
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "8523121D-31F2-4E38-8EA3-33901F094897",
						"kind": "group",
						"name": "assignee",
						"originalName": "assignee",
						"maskFrame": null,
						"layerFrame": {
							"x": 18,
							"y": 74,
							"width": 259,
							"height": 55
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-assignee-oduymzey.png",
							"frame": {
								"x": 18,
								"y": 74,
								"width": 259,
								"height": 55
							}
						},
						"children": []
					},
					{
						"objectId": "8F863B40-66E7-4EE8-B5F4-D638D48CCC0B",
						"kind": "group",
						"name": "milestone",
						"originalName": "milestone",
						"maskFrame": null,
						"layerFrame": {
							"x": 18,
							"y": 147,
							"width": 259,
							"height": 55
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-milestone-oey4njnc.png",
							"frame": {
								"x": 18,
								"y": 147,
								"width": 259,
								"height": 55
							}
						},
						"children": []
					},
					{
						"objectId": "DCD9B65E-9DEB-4679-9D82-94490816A4D4",
						"kind": "group",
						"name": "labels",
						"originalName": "labels",
						"maskFrame": null,
						"layerFrame": {
							"x": 18,
							"y": 220,
							"width": 259,
							"height": 65
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-labels-reneoui2.png",
							"frame": {
								"x": 18,
								"y": 220,
								"width": 259,
								"height": 65
							}
						},
						"children": [
							{
								"objectId": "C87A277C-F7ED-47AD-8252-9C31AEC79BEA",
								"kind": "group",
								"name": "label_frontend",
								"originalName": "label--frontend",
								"maskFrame": null,
								"layerFrame": {
									"x": 18,
									"y": 243,
									"width": 70,
									"height": 25
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-label_frontend-qzg3qti3.png",
									"frame": {
										"x": 18,
										"y": 243,
										"width": 70,
										"height": 25
									}
								},
								"children": []
							},
							{
								"objectId": "2921718D-2CDC-48B5-AFB8-F09248392C03",
								"kind": "group",
								"name": "label_pick_into_stable",
								"originalName": "label--pick-into-stable",
								"maskFrame": null,
								"layerFrame": {
									"x": 96,
									"y": 243,
									"width": 104,
									"height": 25
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-label_pick_into_stable-mjkymtcx.png",
									"frame": {
										"x": 96,
										"y": 243,
										"width": 104,
										"height": 25
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "72169A70-B14D-4C38-AAFD-A4F3197FB9D3",
						"kind": "group",
						"name": "participants",
						"originalName": "participants",
						"maskFrame": null,
						"layerFrame": {
							"x": 18,
							"y": 303,
							"width": 259,
							"height": 64
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-participants-nzixnjlb.png",
							"frame": {
								"x": 18,
								"y": 303,
								"width": 259,
								"height": 64
							}
						},
						"children": [
							{
								"objectId": "DE430408-C632-422C-B8BC-70AC74E722FB",
								"kind": "group",
								"name": "user_picture",
								"originalName": "user-picture",
								"maskFrame": {
									"x": 0,
									"y": 0,
									"width": 22,
									"height": 22
								},
								"layerFrame": {
									"x": 18,
									"y": 326,
									"width": 24,
									"height": 24
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-user_picture-reu0mza0.png",
									"frame": {
										"x": 18,
										"y": 326,
										"width": 24,
										"height": 24
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "E5B5D903-0E14-47DF-AF9C-5E43746B0CED",
						"kind": "group",
						"name": "notifications",
						"originalName": "notifications",
						"maskFrame": null,
						"layerFrame": {
							"x": 10,
							"y": 377,
							"width": 270,
							"height": 94
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-notifications-rtvcnuq5.png",
							"frame": {
								"x": 10,
								"y": 377,
								"width": 270,
								"height": 94
							}
						},
						"children": [
							{
								"objectId": "81391027-B054-4567-B079-16500A61218C",
								"kind": "group",
								"name": "button",
								"originalName": "button",
								"maskFrame": null,
								"layerFrame": {
									"x": 194,
									"y": 377,
									"width": 83,
									"height": 35
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-button-odezotew.png",
									"frame": {
										"x": 194,
										"y": 377,
										"width": 83,
										"height": 35
									}
								},
								"children": []
							}
						]
					}
				]
			}
		]
	},
	{
		"objectId": "CF82899D-8572-4213-819D-75550C242C2B",
		"kind": "artboard",
		"name": "MR",
		"originalName": "MR",
		"maskFrame": null,
		"layerFrame": {
			"x": 2760,
			"y": 102,
			"width": 990,
			"height": 765
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "3A0096D4-E6C7-4412-85F9-529491C37405",
				"kind": "group",
				"name": "mrviewcontent",
				"originalName": "mrviewcontent",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 4,
					"width": 984,
					"height": 1538
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "153689EA-5D68-4E2A-BE19-866026B5AF88",
						"kind": "group",
						"name": "titlebar",
						"originalName": "titlebar",
						"maskFrame": null,
						"layerFrame": {
							"x": 15,
							"y": 4,
							"width": 962,
							"height": 52
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-titlebar-mtuznjg5.png",
							"frame": {
								"x": 15,
								"y": 4,
								"width": 962,
								"height": 52
							}
						},
						"children": [
							{
								"objectId": "4D26AA85-C577-4A19-8A99-0B7091B85482",
								"kind": "group",
								"name": "meta",
								"originalName": "meta",
								"maskFrame": null,
								"layerFrame": {
									"x": 15,
									"y": 16,
									"width": 694,
									"height": 25
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-meta-neqynkfb.png",
									"frame": {
										"x": 15,
										"y": 16,
										"width": 694,
										"height": 25
									}
								},
								"children": [
									{
										"objectId": "7C9E789F-69B0-438E-9E08-678543727C23",
										"kind": "group",
										"name": "Group_19_Copy",
										"originalName": "Group 19 Copy",
										"maskFrame": null,
										"layerFrame": {
											"x": 15,
											"y": 15.0645161290322,
											"width": 232,
											"height": 25
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_19_Copy-n0m5rtc4.png",
											"frame": {
												"x": 15,
												"y": 15.0645161290322,
												"width": 232,
												"height": 25
											}
										},
										"children": [
											{
												"objectId": "5944CF3D-54E1-4885-8421-37BC158BE1D9",
												"kind": "group",
												"name": "Group_10",
												"originalName": "Group 10",
												"maskFrame": null,
												"layerFrame": {
													"x": 23,
													"y": 16,
													"width": 224,
													"height": 24
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_10-ntk0neng.png",
													"frame": {
														"x": 23,
														"y": 16,
														"width": 224,
														"height": 24
													}
												},
												"children": [
													{
														"objectId": "8BCA0BC9-7835-46AB-8151-E123D88C45E2",
														"kind": "group",
														"name": "Group_2",
														"originalName": "Group 2",
														"maskFrame": null,
														"layerFrame": {
															"x": 23,
															"y": 22,
															"width": 51,
															"height": 14
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_2-oejdqtbc.png",
															"frame": {
																"x": 23,
																"y": 22,
																"width": 51,
																"height": 14
															}
														},
														"children": [
															{
																"objectId": "44AE81C5-7266-4F73-8B9A-3B05520D905A",
																"kind": "group",
																"name": "Group_20_Copy",
																"originalName": "Group 20 Copy",
																"maskFrame": null,
																"layerFrame": {
																	"x": 130,
																	"y": 21,
																	"width": 14,
																	"height": 14
																},
																"visible": false,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_20_Copy-ndrbrtgx.png",
																	"frame": {
																		"x": 130,
																		"y": 21,
																		"width": 14,
																		"height": 14
																	}
																},
																"children": []
															},
															{
																"objectId": "63FD733B-65BF-4A27-8CD8-9D89B0A83009",
																"kind": "group",
																"name": "Group_7_Copy_2",
																"originalName": "Group 7 Copy 2",
																"maskFrame": null,
																"layerFrame": {
																	"x": 128,
																	"y": 21,
																	"width": 14,
																	"height": 14
																},
																"visible": false,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_7_Copy_2-njngrdcz.png",
																	"frame": {
																		"x": 128,
																		"y": 21,
																		"width": 14,
																		"height": 14
																	}
																},
																"children": []
															}
														]
													}
												]
											},
											{
												"objectId": "4EA68C51-6200-45E8-BD26-0B2562FB8184",
												"kind": "group",
												"name": "Group",
												"originalName": "Group",
												"maskFrame": null,
												"layerFrame": {
													"x": 192,
													"y": 20,
													"width": 35,
													"height": 16
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"children": []
											}
										]
									},
									{
										"objectId": "24DD9734-F541-4B99-AADE-00DA39E30A9D",
										"kind": "group",
										"name": "Group_19",
										"originalName": "Group 19",
										"maskFrame": null,
										"layerFrame": {
											"x": 15,
											"y": 15.0645161290322,
											"width": 233,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_19-mjrerdk3.png",
											"frame": {
												"x": 15,
												"y": 15.0645161290322,
												"width": 233,
												"height": 25
											}
										},
										"children": [
											{
												"objectId": "999B11AA-697E-4A2C-A4DB-5A4B629ADA0B",
												"kind": "text",
												"name": "ID",
												"originalName": "ID",
												"maskFrame": null,
												"layerFrame": {
													"x": 208,
													"y": 23,
													"width": 33,
													"height": 11
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "!5142",
													"css": [
														"/* !5142: */",
														"font-family: HelveticaNeue;",
														"font-size: 14px;",
														"color: rgba(0,0,0,0.55);",
														"letter-spacing: 0;"
													]
												},
												"image": {
													"path": "images/Layer-ID-otk5qjex.png",
													"frame": {
														"x": 208,
														"y": 23,
														"width": 33,
														"height": 11
													}
												},
												"children": []
											},
											{
												"objectId": "96B1E9D5-EAD7-4CBD-B904-95029E61CD27",
												"kind": "text",
												"name": "Type",
												"originalName": "Type",
												"maskFrame": null,
												"layerFrame": {
													"x": 92,
													"y": 22,
													"width": 100,
													"height": 15
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "Merge Request",
													"css": [
														"/* Merge Request: */",
														"font-family: SourceSansPro-Semibold;",
														"font-size: 16px;",
														"color: #5C5C5C;"
													]
												},
												"image": {
													"path": "images/Layer-Type-otzcmuu5.png",
													"frame": {
														"x": 92,
														"y": 22,
														"width": 100,
														"height": 15
													}
												},
												"children": []
											},
											{
												"objectId": "D147EFAE-85B3-4CCD-B792-99DE7C728E6A",
												"kind": "group",
												"name": "Group_101",
												"originalName": "Group 10",
												"maskFrame": null,
												"layerFrame": {
													"x": 23,
													"y": 16,
													"width": 225,
													"height": 24
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_10-rde0n0vg.png",
													"frame": {
														"x": 23,
														"y": 16,
														"width": 225,
														"height": 24
													}
												},
												"children": [
													{
														"objectId": "6772C134-8C41-4767-9D2F-D201E75F9215",
														"kind": "group",
														"name": "Group_21",
														"originalName": "Group 2",
														"maskFrame": null,
														"layerFrame": {
															"x": 23,
															"y": 22,
															"width": 51,
															"height": 14
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_2-njc3mkmx.png",
															"frame": {
																"x": 23,
																"y": 22,
																"width": 51,
																"height": 14
															}
														},
														"children": [
															{
																"objectId": "FFAC72A4-1252-4A8B-B9AA-9C9B8EE1078F",
																"kind": "group",
																"name": "Group_20_Copy_2",
																"originalName": "Group 20 Copy 2",
																"maskFrame": null,
																"layerFrame": {
																	"x": 130,
																	"y": 21,
																	"width": 14,
																	"height": 14
																},
																"visible": false,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_20_Copy_2-rkzbqzcy.png",
																	"frame": {
																		"x": 130,
																		"y": 21,
																		"width": 14,
																		"height": 14
																	}
																},
																"children": []
															},
															{
																"objectId": "140277CE-BF72-48D4-B925-18A35663A371",
																"kind": "group",
																"name": "Group_7_Copy",
																"originalName": "Group 7 Copy",
																"maskFrame": null,
																"layerFrame": {
																	"x": 128,
																	"y": 21,
																	"width": 14,
																	"height": 14
																},
																"visible": false,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_7_Copy-mtqwmjc3.png",
																	"frame": {
																		"x": 128,
																		"y": 21,
																		"width": 14,
																		"height": 14
																	}
																},
																"children": []
															}
														]
													}
												]
											},
											{
												"objectId": "5D9007E8-8543-4D4D-A8DA-B07AC56A2BF9",
												"kind": "group",
												"name": "Group1",
												"originalName": "Group",
												"maskFrame": null,
												"layerFrame": {
													"x": 192,
													"y": 20,
													"width": 35,
													"height": 16
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"children": []
											}
										]
									},
									{
										"objectId": "C1AF1274-863C-4EED-8DA0-D98537DD02E2",
										"kind": "group",
										"name": "label_merge",
										"originalName": "label-merge",
										"maskFrame": null,
										"layerFrame": {
											"x": 16,
											"y": 15.0645161290322,
											"width": 68,
											"height": 25
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-label_merge-qzfbrjey.png",
											"frame": {
												"x": 16,
												"y": 15.0645161290322,
												"width": 68,
												"height": 25
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "7842A416-D4D7-4A6E-8BAD-D45A03A8C31E",
								"kind": "group",
								"name": "button_edit",
								"originalName": "button-edit",
								"maskFrame": null,
								"layerFrame": {
									"x": 927,
									"y": 10,
									"width": 47,
									"height": 34
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-button_edit-nzg0mke0.png",
									"frame": {
										"x": 927,
										"y": 10,
										"width": 47,
										"height": 34
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "99CD50E8-D489-4D81-A874-E71D78FEB0A2",
						"kind": "group",
						"name": "branches",
						"originalName": "branches",
						"maskFrame": null,
						"layerFrame": {
							"x": 0,
							"y": 178,
							"width": 979,
							"height": 194
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-branches-otldrduw.png",
							"frame": {
								"x": 0,
								"y": 178,
								"width": 979,
								"height": 194
							}
						},
						"children": [
							{
								"objectId": "F876A308-0E20-4D9E-B4BC-0C0BD640BD43",
								"kind": "text",
								"name": "collapse_icon_focus",
								"originalName": "collapse_icon_focus",
								"maskFrame": null,
								"layerFrame": {
									"x": 489,
									"y": 260,
									"width": 13,
									"height": 19
								},
								"visible": false,
								"metadata": {
									"opacity": 1,
									"string": "",
									"css": [
										"/* : */",
										"font-family: FontAwesome;",
										"font-size: 19px;",
										"color: #0069BC;",
										"letter-spacing: 0;"
									]
								},
								"image": {
									"path": "images/Layer-collapse_icon_focus-rjg3nkez.png",
									"frame": {
										"x": 489,
										"y": 260,
										"width": 13,
										"height": 19
									}
								},
								"children": []
							},
							{
								"objectId": "FCB0FDE2-A7E2-417E-8CB2-B8805DC2157A",
								"kind": "text",
								"name": "collapse_icon",
								"originalName": "collapse_icon",
								"maskFrame": null,
								"layerFrame": {
									"x": 489,
									"y": 266,
									"width": 12,
									"height": 7
								},
								"visible": true,
								"metadata": {
									"opacity": 1,
									"string": "",
									"css": [
										"/* : */",
										"opacity: 0.6;",
										"font-family: FontAwesome;",
										"font-size: 19px;",
										"color: #000000;",
										"letter-spacing: 0;"
									]
								},
								"image": {
									"path": "images/Layer-collapse_icon-rkncmeze.png",
									"frame": {
										"x": 489,
										"y": 266,
										"width": 12,
										"height": 7
									}
								},
								"children": []
							},
							{
								"objectId": "0524DF28-74DE-491D-96AC-273CBA49DEF9",
								"kind": "group",
								"name": "branch_id",
								"originalName": "branch-id",
								"maskFrame": null,
								"layerFrame": {
									"x": 15,
									"y": 329,
									"width": 317,
									"height": 24
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "A405FEA3-4BBF-4873-BF13-A46B7D7EB433",
										"kind": "group",
										"name": "Group_27_Copy",
										"originalName": "Group 27 Copy",
										"maskFrame": null,
										"layerFrame": {
											"x": 248,
											"y": 329,
											"width": 84,
											"height": 24
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_27_Copy-qtqwnuzf.png",
											"frame": {
												"x": 248,
												"y": 329,
												"width": 84,
												"height": 24
											}
										},
										"children": []
									},
									{
										"objectId": "7D508049-38A7-4C1C-9C05-15712164FBB8",
										"kind": "group",
										"name": "Group_27",
										"originalName": "Group 27",
										"maskFrame": null,
										"layerFrame": {
											"x": 15,
											"y": 329,
											"width": 221,
											"height": 24
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_27-n0q1mdgw.png",
											"frame": {
												"x": 15,
												"y": 329,
												"width": 221,
												"height": 24
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "BA3E2A10-B1EA-4065-80A9-2738E6420073",
								"kind": "group",
								"name": "divider_focus",
								"originalName": "divider_focus",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 286,
									"width": 958,
									"height": 1
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-divider_focus-qkezrtjb.png",
									"frame": {
										"x": 16,
										"y": 286,
										"width": 958,
										"height": 1
									}
								},
								"children": []
							},
							{
								"objectId": "C270820C-3E90-4866-826A-F7A5BF3DB6D4",
								"kind": "group",
								"name": "divider",
								"originalName": "divider",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 286,
									"width": 958,
									"height": 1
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-divider-qzi3mdgy.png",
									"frame": {
										"x": 16,
										"y": 286,
										"width": 958,
										"height": 1
									}
								},
								"children": []
							},
							{
								"objectId": "C211F1E6-BC8D-4B99-B8E4-29F4A5A33E5A",
								"kind": "group",
								"name": "collapse_gradient_focus",
								"originalName": "collapse_gradient_focus",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 176,
									"width": 958,
									"height": 110
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-collapse_gradient_focus-qzixmuyx.png",
									"frame": {
										"x": 16,
										"y": 176,
										"width": 958,
										"height": 110
									}
								},
								"children": []
							},
							{
								"objectId": "92A9D402-F7FD-459B-A592-9C27F0E3EE91",
								"kind": "group",
								"name": "collapse_gradient",
								"originalName": "collapse_gradient",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 178,
									"width": 958,
									"height": 108
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-collapse_gradient-otjbouq0.png",
									"frame": {
										"x": 16,
										"y": 178,
										"width": 958,
										"height": 108
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "A9657117-DC83-4A12-A852-FD5638D1009C",
						"kind": "group",
						"name": "tab_tabs",
						"originalName": "tab_tabs",
						"maskFrame": null,
						"layerFrame": {
							"x": 16,
							"y": 434,
							"width": 958,
							"height": 51
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-tab_tabs-qtk2ntcx.png",
							"frame": {
								"x": 16,
								"y": 434,
								"width": 958,
								"height": 51
							}
						},
						"children": [
							{
								"objectId": "B5204017-B58F-4787-8A1A-E40E58EE65E6",
								"kind": "group",
								"name": "unresolved_discussions",
								"originalName": "unresolved_discussions",
								"maskFrame": null,
								"layerFrame": {
									"x": 724,
									"y": 443,
									"width": 250,
									"height": 30
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-unresolved_discussions-qjuymdqw.jpg",
									"frame": {
										"x": 724,
										"y": 443,
										"width": 250,
										"height": 30
									}
								},
								"children": [
									{
										"objectId": "6F6AF7D2-4D8C-4BC3-8F7B-810DAAF5E856",
										"kind": "group",
										"name": "Group_28",
										"originalName": "Group 28",
										"maskFrame": null,
										"layerFrame": {
											"x": 724,
											"y": 443,
											"width": 250,
											"height": 30
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_28-nky2quy3.png",
											"frame": {
												"x": 724,
												"y": 443,
												"width": 250,
												"height": 30
											}
										},
										"children": [
											{
												"objectId": "C85ABB2E-62A6-44B8-B4AD-C7012A952FE2",
												"kind": "group",
												"name": "Group_9_Copy_8",
												"originalName": "Group 9 Copy 8",
												"maskFrame": null,
												"layerFrame": {
													"x": 734,
													"y": 451,
													"width": 14,
													"height": 15
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_9_Copy_8-qzg1qujc.png",
													"frame": {
														"x": 734,
														"y": 451,
														"width": 14,
														"height": 15
													}
												},
												"children": []
											}
										]
									}
								]
							},
							{
								"objectId": "D010F1BD-4C4C-421B-99FD-F60639352DDE",
								"kind": "group",
								"name": "merge_status",
								"originalName": "merge_status",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 434,
									"width": 151,
									"height": 50
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "0E9A010A-3A1D-4788-926E-0B428D0A32B1",
										"kind": "text",
										"name": "title",
										"originalName": "title",
										"maskFrame": null,
										"layerFrame": {
											"x": 24,
											"y": 453,
											"width": 78,
											"height": 14
										},
										"visible": true,
										"metadata": {
											"opacity": 1,
											"string": "Merge status",
											"css": [
												"/* title: */",
												"font-family: SourceSansPro-Regular;",
												"font-size: 15px;",
												"color: #000000;"
											]
										},
										"image": {
											"path": "images/Layer-title-meu5qtax.png",
											"frame": {
												"x": 24,
												"y": 453,
												"width": 78,
												"height": 14
											}
										},
										"children": []
									},
									{
										"objectId": "0AF4584A-5E78-4A55-95B4-6A43C7953993",
										"kind": "group",
										"name": "mr_underline",
										"originalName": "mr_underline",
										"maskFrame": null,
										"layerFrame": {
											"x": 16,
											"y": 482,
											"width": 151,
											"height": 2
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-mr_underline-mefgndu4.png",
											"frame": {
												"x": 16,
												"y": 482,
												"width": 151,
												"height": 2
											}
										},
										"children": []
									},
									{
										"objectId": "AC54923E-D7F1-499C-987C-1FF6E06D6494",
										"kind": "group",
										"name": "mini_mrstatus",
										"originalName": "mini-mrstatus",
										"maskFrame": null,
										"layerFrame": {
											"x": 110,
											"y": 449,
											"width": 41,
											"height": 20
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"children": [
											{
												"objectId": "14518819-0FA5-49AB-9506-23EEB4305E42",
												"kind": "group",
												"name": "mini_status_pipeline",
												"originalName": "mini-status-pipeline",
												"maskFrame": null,
												"layerFrame": {
													"x": 110,
													"y": 449,
													"width": 20,
													"height": 20
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-mini_status_pipeline-mtq1mtg4.png",
													"frame": {
														"x": 110,
														"y": 449,
														"width": 20,
														"height": 20
													}
												},
												"children": [
													{
														"objectId": "87B53062-FF74-4664-8682-9847F35C6719",
														"kind": "group",
														"name": "pipelinetooltip",
														"originalName": "pipelinetooltip",
														"maskFrame": null,
														"layerFrame": {
															"x": 32,
															"y": 419,
															"width": 186,
															"height": 29
														},
														"visible": false,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-pipelinetooltip-oddcntmw.png",
															"frame": {
																"x": 32,
																"y": 419,
																"width": 186,
																"height": 29
															}
														},
														"children": []
													}
												]
											},
											{
												"objectId": "3EA93972-3C4D-4FDD-9B7F-E2195D7AC187",
												"kind": "group",
												"name": "mini_status_codequality",
												"originalName": "mini-status-codequality",
												"maskFrame": null,
												"layerFrame": {
													"x": 120,
													"y": 449,
													"width": 21,
													"height": 20
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-mini_status_codequality-m0vbotm5.png",
													"frame": {
														"x": 120,
														"y": 449,
														"width": 21,
														"height": 20
													}
												},
												"children": [
													{
														"objectId": "2E375218-FAE3-4B14-8F62-E6194A8A15D5",
														"kind": "group",
														"name": "codequalitytooltip",
														"originalName": "codequalitytooltip",
														"maskFrame": null,
														"layerFrame": {
															"x": 35,
															"y": 419,
															"width": 199,
															"height": 29
														},
														"visible": false,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-codequalitytooltip-mkuznzuy.png",
															"frame": {
																"x": 35,
																"y": 419,
																"width": 199,
																"height": 29
															}
														},
														"children": []
													}
												]
											},
											{
												"objectId": "9A4B8A41-B652-4877-8252-815530EC3F82",
												"kind": "group",
												"name": "mini_status_mergability",
												"originalName": "mini-status-mergability",
												"maskFrame": null,
												"layerFrame": {
													"x": 131,
													"y": 449,
													"width": 20,
													"height": 20
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-mini_status_mergability-oue0qjhb.png",
													"frame": {
														"x": 131,
														"y": 449,
														"width": 20,
														"height": 20
													}
												},
												"children": [
													{
														"objectId": "224B1125-B632-4803-97AD-7E25E0EFF692",
														"kind": "group",
														"name": "mergabilitytooltip",
														"originalName": "mergabilitytooltip",
														"maskFrame": null,
														"layerFrame": {
															"x": 46,
															"y": 419,
															"width": 199,
															"height": 29
														},
														"visible": false,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-mergabilitytooltip-mji0qjex.png",
															"frame": {
																"x": 46,
																"y": 419,
																"width": 199,
																"height": 29
															}
														},
														"children": []
													}
												]
											}
										]
									},
									{
										"objectId": "172DB987-81CE-4025-87B9-5FD68FF8FDCF",
										"kind": "group",
										"name": "bg",
										"originalName": "bg",
										"maskFrame": null,
										"layerFrame": {
											"x": 16,
											"y": 434,
											"width": 151,
											"height": 50
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-bg-mtcyrei5.png",
											"frame": {
												"x": 16,
												"y": 434,
												"width": 151,
												"height": 50
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "3B1035F6-2210-4A32-859B-E455641689B4",
								"kind": "group",
								"name": "discussion",
								"originalName": "discussion",
								"maskFrame": null,
								"layerFrame": {
									"x": 167,
									"y": 434,
									"width": 111,
									"height": 50
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "549AFAD4-9F28-4C4C-A8BA-9D8AA0315E19",
										"kind": "text",
										"name": "title1",
										"originalName": "title",
										"maskFrame": null,
										"layerFrame": {
											"x": 178,
											"y": 452,
											"width": 65,
											"height": 12
										},
										"visible": true,
										"metadata": {
											"opacity": 1,
											"string": "Discussion",
											"css": [
												"/* title: */",
												"font-family: SourceSansPro-Regular;",
												"font-size: 15px;",
												"color: #000000;"
											]
										},
										"image": {
											"path": "images/Layer-title-ntq5quzb.png",
											"frame": {
												"x": 178,
												"y": 452,
												"width": 65,
												"height": 12
											}
										},
										"children": []
									},
									{
										"objectId": "6DFEED51-823E-4DBF-BE83-8FFCEF68DD9A",
										"kind": "group",
										"name": "underline",
										"originalName": "underline",
										"maskFrame": null,
										"layerFrame": {
											"x": 167,
											"y": 482,
											"width": 111,
											"height": 2
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-underline-nkrgruve.png",
											"frame": {
												"x": 167,
												"y": 482,
												"width": 111,
												"height": 2
											}
										},
										"children": []
									},
									{
										"objectId": "1E99FE82-50D7-4565-B5B9-72E03F6A5DD5",
										"kind": "group",
										"name": "count_badge",
										"originalName": "count-badge",
										"maskFrame": null,
										"layerFrame": {
											"x": 247,
											"y": 450,
											"width": 23,
											"height": 19
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-count_badge-muu5ouzf.png",
											"frame": {
												"x": 247,
												"y": 450,
												"width": 23,
												"height": 19
											}
										},
										"children": []
									},
									{
										"objectId": "AC293203-58F3-4C9D-82D4-A9E9B20B1466",
										"kind": "group",
										"name": "bg1",
										"originalName": "bg",
										"maskFrame": null,
										"layerFrame": {
											"x": 167,
											"y": 434,
											"width": 111,
											"height": 50
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-bg-qumyotmy.png",
											"frame": {
												"x": 167,
												"y": 434,
												"width": 111,
												"height": 50
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "BC3A40EB-7312-4658-A940-346297CB7F7D",
								"kind": "group",
								"name": "commits",
								"originalName": "commits",
								"maskFrame": null,
								"layerFrame": {
									"x": 278,
									"y": 434,
									"width": 101,
									"height": 50
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "76A26FEF-0A53-4B9E-BF6B-2EDE6C40C290",
										"kind": "text",
										"name": "title2",
										"originalName": "title",
										"maskFrame": null,
										"layerFrame": {
											"x": 288,
											"y": 452,
											"width": 56,
											"height": 12
										},
										"visible": true,
										"metadata": {
											"opacity": 1,
											"string": "Commits",
											"css": [
												"/* title: */",
												"font-family: SourceSansPro-Regular;",
												"font-size: 15px;",
												"color: #000000;"
											]
										},
										"image": {
											"path": "images/Layer-title-nzzbmjzg.png",
											"frame": {
												"x": 288,
												"y": 452,
												"width": 56,
												"height": 12
											}
										},
										"children": []
									},
									{
										"objectId": "3699D096-F012-4309-89A8-30708F533329",
										"kind": "group",
										"name": "underline1",
										"originalName": "underline",
										"maskFrame": null,
										"layerFrame": {
											"x": 278,
											"y": 482,
											"width": 101,
											"height": 2
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-underline-mzy5ouqw.png",
											"frame": {
												"x": 278,
												"y": 482,
												"width": 101,
												"height": 2
											}
										},
										"children": []
									},
									{
										"objectId": "C1269F7D-2C07-4032-846C-0E4CA9E9E2AF",
										"kind": "group",
										"name": "count_badge1",
										"originalName": "count-badge",
										"maskFrame": null,
										"layerFrame": {
											"x": 348,
											"y": 450,
											"width": 21,
											"height": 19
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-count_badge-qzeynjlg.png",
											"frame": {
												"x": 348,
												"y": 450,
												"width": 21,
												"height": 19
											}
										},
										"children": []
									},
									{
										"objectId": "C2FDB910-DC13-4AB6-BF5B-3720E2444634",
										"kind": "group",
										"name": "bg2",
										"originalName": "bg",
										"maskFrame": null,
										"layerFrame": {
											"x": 278,
											"y": 434,
											"width": 101,
											"height": 50
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-bg-qzjgrei5.png",
											"frame": {
												"x": 278,
												"y": 434,
												"width": 101,
												"height": 50
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "2C46EB81-10AD-41CB-A311-E733D1EF2541",
								"kind": "group",
								"name": "pipelines",
								"originalName": "pipelines",
								"maskFrame": null,
								"layerFrame": {
									"x": 379,
									"y": 434,
									"width": 102,
									"height": 50
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "973E2762-0E1A-4C58-AF33-408BA15EAD12",
										"kind": "text",
										"name": "title3",
										"originalName": "title",
										"maskFrame": null,
										"layerFrame": {
											"x": 390,
											"y": 452,
											"width": 56,
											"height": 15
										},
										"visible": true,
										"metadata": {
											"opacity": 1,
											"string": "Pipelines",
											"css": [
												"/* title: */",
												"font-family: SourceSansPro-Regular;",
												"font-size: 15px;",
												"color: #000000;"
											]
										},
										"image": {
											"path": "images/Layer-title-otczrti3.png",
											"frame": {
												"x": 390,
												"y": 452,
												"width": 56,
												"height": 15
											}
										},
										"children": []
									},
									{
										"objectId": "F3E2E275-DF7B-4EC8-B9AB-8BACA7C6F236",
										"kind": "group",
										"name": "underline2",
										"originalName": "underline",
										"maskFrame": null,
										"layerFrame": {
											"x": 379,
											"y": 482,
											"width": 102,
											"height": 2
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-underline-rjnfmkuy.png",
											"frame": {
												"x": 379,
												"y": 482,
												"width": 102,
												"height": 2
											}
										},
										"children": []
									},
									{
										"objectId": "4AE7F9BF-6540-4D9B-8E68-245EC9D4FC75",
										"kind": "group",
										"name": "count_badge2",
										"originalName": "count-badge",
										"maskFrame": null,
										"layerFrame": {
											"x": 450,
											"y": 450,
											"width": 21,
											"height": 19
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-count_badge-neffn0y5.png",
											"frame": {
												"x": 450,
												"y": 450,
												"width": 21,
												"height": 19
											}
										},
										"children": []
									},
									{
										"objectId": "167CC35C-D5AA-4ABA-BACA-EBAB80D321D6",
										"kind": "group",
										"name": "bg3",
										"originalName": "bg",
										"maskFrame": null,
										"layerFrame": {
											"x": 379,
											"y": 434,
											"width": 102,
											"height": 50
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-bg-mty3q0mz.png",
											"frame": {
												"x": 379,
												"y": 434,
												"width": 102,
												"height": 50
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "F7609AE4-9CDD-4A6B-95C6-77ECEBF8C2A0",
								"kind": "group",
								"name": "changes",
								"originalName": "changes",
								"maskFrame": null,
								"layerFrame": {
									"x": 481,
									"y": 434,
									"width": 100,
									"height": 50
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "E338EECA-7FAC-4A82-9B12-944D81924B81",
										"kind": "text",
										"name": "title4",
										"originalName": "title",
										"maskFrame": null,
										"layerFrame": {
											"x": 491,
											"y": 452,
											"width": 53,
											"height": 15
										},
										"visible": true,
										"metadata": {
											"opacity": 1,
											"string": "Changes",
											"css": [
												"/* title: */",
												"font-family: SourceSansPro-Regular;",
												"font-size: 15px;",
												"color: #000000;"
											]
										},
										"image": {
											"path": "images/Layer-title-rtmzoevf.png",
											"frame": {
												"x": 491,
												"y": 452,
												"width": 53,
												"height": 15
											}
										},
										"children": []
									},
									{
										"objectId": "84D40848-8670-4455-9DF6-061DB284E866",
										"kind": "group",
										"name": "underline3",
										"originalName": "underline",
										"maskFrame": null,
										"layerFrame": {
											"x": 481,
											"y": 482,
											"width": 100,
											"height": 2
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-underline-odrenda4.png",
											"frame": {
												"x": 481,
												"y": 482,
												"width": 100,
												"height": 2
											}
										},
										"children": []
									},
									{
										"objectId": "35150EB4-2CFF-4F3A-8959-5E7FDE5DEE58",
										"kind": "group",
										"name": "count_badge3",
										"originalName": "count-badge",
										"maskFrame": null,
										"layerFrame": {
											"x": 548,
											"y": 450,
											"width": 21,
											"height": 19
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-count_badge-mzuxntbf.png",
											"frame": {
												"x": 548,
												"y": 450,
												"width": 21,
												"height": 19
											}
										},
										"children": []
									},
									{
										"objectId": "8ABCF9F3-F99D-4998-8572-47F11A5044D8",
										"kind": "group",
										"name": "bg4",
										"originalName": "bg",
										"maskFrame": null,
										"layerFrame": {
											"x": 481,
											"y": 434,
											"width": 100,
											"height": 50
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-bg-oefcq0y5.png",
											"frame": {
												"x": 481,
												"y": 434,
												"width": 100,
												"height": 50
											}
										},
										"children": []
									}
								]
							}
						]
					},
					{
						"objectId": "A5E2F603-FEE5-4CA4-902B-BA072B31AEDC",
						"kind": "group",
						"name": "tab_content",
						"originalName": "tab_content",
						"maskFrame": null,
						"layerFrame": {
							"x": 16,
							"y": 485,
							"width": 959,
							"height": 1057
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-tab_content-qtvfmky2.png",
							"frame": {
								"x": 16,
								"y": 485,
								"width": 959,
								"height": 1057
							}
						},
						"children": [
							{
								"objectId": "06822FFB-5814-4F2F-89EB-FC39CDC098D5",
								"kind": "group",
								"name": "discussion_tab",
								"originalName": "discussion_tab",
								"maskFrame": null,
								"layerFrame": {
									"x": 12,
									"y": 486,
									"width": 979,
									"height": 1075
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-discussion_tab-mdy4mjjg.jpg",
									"frame": {
										"x": 12,
										"y": 486,
										"width": 979,
										"height": 1075
									}
								},
								"children": []
							},
							{
								"objectId": "3F4D8E29-2A7E-447D-A2E5-67C056EEE172",
								"kind": "group",
								"name": "commits_tab",
								"originalName": "commits_tab",
								"maskFrame": null,
								"layerFrame": {
									"x": 10,
									"y": 484,
									"width": 970,
									"height": 91
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-commits_tab-m0y0rdhf.jpg",
									"frame": {
										"x": 10,
										"y": 484,
										"width": 970,
										"height": 91
									}
								},
								"children": []
							},
							{
								"objectId": "E6D94B02-2571-474D-BC60-EB8E30E289D6",
								"kind": "group",
								"name": "pipelines_tab",
								"originalName": "pipelines_tab",
								"maskFrame": null,
								"layerFrame": {
									"x": 5,
									"y": 486,
									"width": 977,
									"height": 170
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-pipelines_tab-rtzeotrc.jpg",
									"frame": {
										"x": 5,
										"y": 486,
										"width": 977,
										"height": 170
									}
								},
								"children": []
							},
							{
								"objectId": "432D8566-686D-460F-8FDF-D7F691A58E85",
								"kind": "group",
								"name": "changes_tab",
								"originalName": "changes_tab",
								"maskFrame": null,
								"layerFrame": {
									"x": -16,
									"y": 483,
									"width": 1027,
									"height": 9120
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-changes_tab-ndmyrdg1.jpg",
									"frame": {
										"x": -16,
										"y": 483,
										"width": 1027,
										"height": 9120
									}
								},
								"children": []
							},
							{
								"objectId": "C31AB20B-B37E-4AB2-A62B-9D998C1CD1AE",
								"kind": "group",
								"name": "mergestatus_tab",
								"originalName": "mergestatus_tab",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 500,
									"width": 958,
									"height": 242
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-mergestatus_tab-qzmxquiy.png",
									"frame": {
										"x": 16,
										"y": 500,
										"width": 958,
										"height": 242
									}
								},
								"children": [
									{
										"objectId": "78E1E10F-97EC-44C2-8A67-53ADAE38152B",
										"kind": "group",
										"name": "stop_environment_button_copy_2",
										"originalName": "stop-environment-button copy 2",
										"maskFrame": null,
										"layerFrame": {
											"x": 574,
											"y": 614,
											"width": 99,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-stop_environment_button_copy_2-nzhfmuux.png",
											"frame": {
												"x": 574,
												"y": 614,
												"width": 99,
												"height": 25
											}
										},
										"children": []
									},
									{
										"objectId": "70A6FE50-173B-407B-9E7E-399F6CCDA03A",
										"kind": "group",
										"name": "Group_23",
										"originalName": "Group 23",
										"maskFrame": null,
										"layerFrame": {
											"x": 509,
											"y": 614,
											"width": 55,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 0.35
										},
										"image": {
											"path": "images/Layer-Group_23-nzbbnkzf.png",
											"frame": {
												"x": 509,
												"y": 614,
												"width": 55,
												"height": 25
											}
										},
										"children": []
									},
									{
										"objectId": "EDDACCBB-49A2-4347-AD3F-9BF4664C9841",
										"kind": "text",
										"name": "command_line_instructions",
										"originalName": "command-line-instructions",
										"maskFrame": null,
										"layerFrame": {
											"x": 68,
											"y": 727,
											"width": 466,
											"height": 15
										},
										"visible": true,
										"metadata": {
											"opacity": 1,
											"string": "You can also merge or inspect manually using the command line instructions",
											"css": [
												"/* You can also merge o: */",
												"font-family: SourceSansPro-It;",
												"font-size: 15px;",
												"color: #333333;",
												"letter-spacing: 0;",
												"line-height: 20px;"
											]
										},
										"image": {
											"path": "images/Layer-command_line_instructions-rureqund.png",
											"frame": {
												"x": 68,
												"y": 727,
												"width": 466,
												"height": 15
											}
										},
										"children": []
									},
									{
										"objectId": "2275CC7D-1F44-4E9F-97B3-97FF0DBE2ADF",
										"kind": "group",
										"name": "Group_9_Copy_5",
										"originalName": "Group 9 Copy 5",
										"maskFrame": null,
										"layerFrame": {
											"x": 31,
											"y": 502,
											"width": 22,
											"height": 22
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_9_Copy_5-mji3nund.png",
											"frame": {
												"x": 31,
												"y": 502,
												"width": 22,
												"height": 22
											}
										},
										"children": []
									},
									{
										"objectId": "7B3556D4-AFE6-43BB-97B7-FA7CB51D7305",
										"kind": "group",
										"name": "Group_9",
										"originalName": "Group 9",
										"maskFrame": null,
										"layerFrame": {
											"x": 31,
											"y": 615,
											"width": 22,
											"height": 22
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_9-n0izntu2.png",
											"frame": {
												"x": 31,
												"y": 615,
												"width": 22,
												"height": 22
											}
										},
										"children": []
									},
									{
										"objectId": "D0C98100-889F-452C-9363-74B46DEFA107",
										"kind": "group",
										"name": "Group_18",
										"originalName": "Group 18",
										"maskFrame": null,
										"layerFrame": {
											"x": 16,
											"y": 500,
											"width": 958,
											"height": 213
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_18-rdbdotgx.png",
											"frame": {
												"x": 16,
												"y": 500,
												"width": 958,
												"height": 213
											}
										},
										"children": [
											{
												"objectId": "644F10B8-1079-4573-9166-EA5A2B5AEE79",
												"kind": "group",
												"name": "Group_211",
												"originalName": "Group 21",
												"maskFrame": null,
												"layerFrame": {
													"x": 31,
													"y": 500,
													"width": 387,
													"height": 27
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_21-njq0rjew.png",
													"frame": {
														"x": 31,
														"y": 500,
														"width": 387,
														"height": 27
													}
												},
												"children": [
													{
														"objectId": "B47363B7-FAE5-4C99-875F-02DE344CE69D",
														"kind": "group",
														"name": "Group_9_Copy_3",
														"originalName": "Group 9 Copy 3",
														"maskFrame": null,
														"layerFrame": {
															"x": 31,
															"y": 502,
															"width": 22,
															"height": 22
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_9_Copy_3-qjq3mzyz.png",
															"frame": {
																"x": 31,
																"y": 502,
																"width": 22,
																"height": 22
															}
														},
														"children": []
													}
												]
											},
											{
												"objectId": "82699F9A-7121-4F27-AF33-F6E19DD7F254",
												"kind": "group",
												"name": "Group_20",
												"originalName": "Group 20",
												"maskFrame": null,
												"layerFrame": {
													"x": 17,
													"y": 532,
													"width": 956,
													"height": 38
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_20-odi2otlg.png",
													"frame": {
														"x": 17,
														"y": 532,
														"width": 956,
														"height": 38
													}
												},
												"children": [
													{
														"objectId": "15EE24CD-5644-4F90-BA07-38ADB37D7583",
														"kind": "group",
														"name": "Group_22",
														"originalName": "Group 22",
														"maskFrame": null,
														"layerFrame": {
															"x": 58,
															"y": 541,
															"width": 62,
															"height": 22
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_22-mtvfrti0.png",
															"frame": {
																"x": 58,
																"y": 541,
																"width": 62,
																"height": 22
															}
														},
														"children": []
													}
												]
											},
											{
												"objectId": "21B7A3EA-02EE-4C7F-BE9E-882A844F6E7F",
												"kind": "group",
												"name": "MR_body_text",
												"originalName": "MR-body-text",
												"maskFrame": null,
												"layerFrame": {
													"x": 67,
													"y": 564,
													"width": 206,
													"height": 15
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-MR_body_text-mjfcn0ez.png",
													"frame": {
														"x": 67,
														"y": 564,
														"width": 206,
														"height": 15
													}
												},
												"children": [
													{
														"objectId": "181C1B40-FF18-44B1-BD2A-537B03AE46CB",
														"kind": "group",
														"name": "stop_environment_button_copy",
														"originalName": "stop-environment-button copy",
														"maskFrame": null,
														"layerFrame": {
															"x": 448,
															"y": 553,
															"width": 130,
															"height": 24
														},
														"visible": false,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-stop_environment_button_copy-mtgxqzfc.png",
															"frame": {
																"x": 448,
																"y": 553,
																"width": 130,
																"height": 24
															}
														},
														"children": []
													},
													{
														"objectId": "8BC0C062-883F-4386-85F1-AA4238DCEBE7",
														"kind": "group",
														"name": "environment_buttons",
														"originalName": "environment buttons",
														"maskFrame": null,
														"layerFrame": {
															"x": 62,
															"y": 585,
															"width": 475,
															"height": 127
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"children": [
															{
																"objectId": "86DE8F6F-7E50-49DB-8D2F-83950B57BEBD",
																"kind": "group",
																"name": "Group_14",
																"originalName": "Group 14",
																"maskFrame": null,
																"layerFrame": {
																	"x": 481,
																	"y": 585,
																	"width": 56,
																	"height": 26
																},
																"visible": false,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "B462A713-B976-495B-B558-4EBAAADD966E",
																		"kind": "group",
																		"name": "stop_environment_button_copy_3",
																		"originalName": "stop-environment-button copy 3",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 511,
																			"y": 586,
																			"width": 26,
																			"height": 25
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-stop_environment_button_copy_3-qjq2mke3.png",
																			"frame": {
																				"x": 511,
																				"y": 586,
																				"width": 26,
																				"height": 25
																			}
																		},
																		"children": []
																	},
																	{
																		"objectId": "11EAFAD2-7600-4FD3-B8CC-B11994B1B294",
																		"kind": "group",
																		"name": "stop_environment_button_copy_4",
																		"originalName": "stop-environment-button copy 4",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 481,
																			"y": 586,
																			"width": 31,
																			"height": 25
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-stop_environment_button_copy_4-mtffquzb.png",
																			"frame": {
																				"x": 481,
																				"y": 586,
																				"width": 31,
																				"height": 25
																			}
																		},
																		"children": []
																	}
																]
															},
															{
																"objectId": "8F44467B-1701-4284-8613-1C2874314CB2",
																"kind": "group",
																"name": "Group_13_Copy",
																"originalName": "Group 13 Copy",
																"maskFrame": null,
																"layerFrame": {
																	"x": 62,
																	"y": 687,
																	"width": 399,
																	"height": 25
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "66D0C1CF-15C2-42D1-B5AC-5E03786A9F34",
																		"kind": "group",
																		"name": "stop_environment_button_copy_5",
																		"originalName": "stop-environment-button copy 5",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 62,
																			"y": 687,
																			"width": 159,
																			"height": 25
																		},
																		"visible": false,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-stop_environment_button_copy_5-njzememx.png",
																			"frame": {
																				"x": 62,
																				"y": 687,
																				"width": 159,
																				"height": 25
																			}
																		},
																		"children": []
																	},
																	{
																		"objectId": "8D23F683-523E-40EC-ACEC-25F90D497213",
																		"kind": "group",
																		"name": "stop_environment_button_copy_21",
																		"originalName": "stop-environment-button copy 2",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 302,
																			"y": 687,
																			"width": 159,
																			"height": 25
																		},
																		"visible": false,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-stop_environment_button_copy_2-oeqym0y2.png",
																			"frame": {
																				"x": 302,
																				"y": 687,
																				"width": 159,
																				"height": 25
																			}
																		},
																		"children": []
																	}
																]
															}
														]
													},
													{
														"objectId": "294EA39D-B586-4EB5-9033-C60849A22788",
														"kind": "group",
														"name": "Group_13",
														"originalName": "Group 13",
														"maskFrame": null,
														"layerFrame": {
															"x": 721,
															"y": 499,
															"width": 243,
															"height": 30
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"children": [
															{
																"objectId": "415C476F-0D45-44F4-BBC4-5EC80468EE62",
																"kind": "group",
																"name": "Group_6",
																"originalName": "Group 6",
																"maskFrame": null,
																"layerFrame": {
																	"x": 721,
																	"y": 499,
																	"width": 243,
																	"height": 30
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_6-nde1qzq3.png",
																	"frame": {
																		"x": 721,
																		"y": 499,
																		"width": 243,
																		"height": 30
																	}
																},
																"children": []
															}
														]
													},
													{
														"objectId": "7E5DBC5F-AB22-41E0-8D87-5BC7B8514B94",
														"kind": "group",
														"name": "Group_4",
														"originalName": "Group 4",
														"maskFrame": null,
														"layerFrame": {
															"x": 477,
															"y": 584,
															"width": 30,
															"height": 30
														},
														"visible": false,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_4-n0u1rejd.png",
															"frame": {
																"x": 477,
																"y": 584,
																"width": 30,
																"height": 30
															}
														},
														"children": []
													}
												]
											},
											{
												"objectId": "ED73A3FD-1CD2-451D-BDCA-6AEB41CA6455",
												"kind": "group",
												"name": "Group_9_Copy_2",
												"originalName": "Group 9 Copy 2",
												"maskFrame": null,
												"layerFrame": {
													"x": 37,
													"y": 623,
													"width": 10,
													"height": 11
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_9_Copy_2-ruq3m0ez.png",
													"frame": {
														"x": 37,
														"y": 623,
														"width": 10,
														"height": 11
													}
												},
												"children": []
											},
											{
												"objectId": "19DCF43F-39CE-4351-A7FA-A40DE2ECDE45",
												"kind": "group",
												"name": "Group_9_Copy",
												"originalName": "Group 9 Copy",
												"maskFrame": null,
												"layerFrame": {
													"x": 31,
													"y": 558,
													"width": 22,
													"height": 22
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_9_Copy-mtleq0y0.png",
													"frame": {
														"x": 31,
														"y": 558,
														"width": 22,
														"height": 22
													}
												},
												"children": []
											}
										]
									}
								]
							}
						]
					},
					{
						"objectId": "A2D31594-CFFB-4BEA-9D48-1255E1611FC0",
						"kind": "group",
						"name": "award_emoji",
						"originalName": "award_emoji",
						"maskFrame": null,
						"layerFrame": {
							"x": 16,
							"y": 372,
							"width": 958,
							"height": 62
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-award_emoji-qtjemze1.png",
							"frame": {
								"x": 16,
								"y": 372,
								"width": 958,
								"height": 62
							}
						},
						"children": [
							{
								"objectId": "28394BE1-B8A6-4AEB-B7D3-D6547600F084",
								"kind": "group",
								"name": "thumbs_down_button",
								"originalName": "thumbs-down-button",
								"maskFrame": null,
								"layerFrame": {
									"x": 72,
									"y": 386,
									"width": 48,
									"height": 33
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-thumbs_down_button-mjgzotrc.png",
									"frame": {
										"x": 72,
										"y": 386,
										"width": 48,
										"height": 33
									}
								},
								"children": [
									{
										"objectId": "8A071A68-4064-4077-BE1A-F2840F51D83A",
										"kind": "group",
										"name": "$1f44e",
										"originalName": "1f44e",
										"maskFrame": null,
										"layerFrame": {
											"x": 82,
											"y": 394,
											"width": 14,
											"height": 19
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"children": [
											{
												"objectId": "291FA0AF-8567-4A27-B8B0-2924850834EF",
												"kind": "group",
												"name": "Group2",
												"originalName": "Group",
												"maskFrame": null,
												"layerFrame": {
													"x": 82.5,
													"y": 394,
													"width": 13,
													"height": 19
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group-mjkxrkew.png",
													"frame": {
														"x": 82.5,
														"y": 394,
														"width": 13,
														"height": 19
													}
												},
												"children": []
											}
										]
									}
								]
							},
							{
								"objectId": "E67789DB-4037-4508-BFEA-6DF5551C4DE6",
								"kind": "group",
								"name": "thumbs_up_button",
								"originalName": "thumbs-up-button",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 386,
									"width": 47,
									"height": 33
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-thumbs_up_button-rty3nzg5.png",
									"frame": {
										"x": 16,
										"y": 386,
										"width": 47,
										"height": 33
									}
								},
								"children": [
									{
										"objectId": "459E3323-009F-4A07-995A-43036E87BFC5",
										"kind": "group",
										"name": "$1f44d",
										"originalName": "1f44d",
										"maskFrame": null,
										"layerFrame": {
											"x": 26,
											"y": 394,
											"width": 13,
											"height": 19
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"children": [
											{
												"objectId": "0D0F33EF-92D6-4612-B58E-67745275D211",
												"kind": "group",
												"name": "Group3",
												"originalName": "Group",
												"maskFrame": null,
												"layerFrame": {
													"x": 26.5,
													"y": 394,
													"width": 13,
													"height": 19
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group-meqwrjmz.png",
													"frame": {
														"x": 26.5,
														"y": 394,
														"width": 13,
														"height": 19
													}
												},
												"children": []
											}
										]
									}
								]
							},
							{
								"objectId": "E0DC97BE-5BE2-47C2-BA1D-741834F1E205",
								"kind": "group",
								"name": "add_emoji_button",
								"originalName": "add-emoji-button",
								"maskFrame": null,
								"layerFrame": {
									"x": 129,
									"y": 386,
									"width": 61,
									"height": 32
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-add_emoji_button-rtbeqzk3.png",
									"frame": {
										"x": 129,
										"y": 386,
										"width": 61,
										"height": 32
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "EA538AB0-124E-4903-973D-AE6CA6AD2CE6",
						"kind": "group",
						"name": "description",
						"originalName": "description",
						"maskFrame": null,
						"layerFrame": {
							"x": 12,
							"y": 63,
							"width": 972,
							"height": 523
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-description-rue1mzhb.jpg",
							"frame": {
								"x": 12,
								"y": 63,
								"width": 972,
								"height": 523
							}
						},
						"children": []
					}
				]
			}
		]
	}
]